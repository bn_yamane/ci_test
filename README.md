ブログ記事 [PHPでCIするTravis CIとdrone.ioを試してみた | bulblub](http://bulblub.com/2013/10/11/php_ci_with_travis-ci_drone-io/ "PHPでCIするTravis CIとdrone.ioを試してみた | bulblub") で共有したリポジトリ

## Travis CI

* [![Build Status](https://travis-ci.org/snize/ci_test.png?branch=master)](https://travis-ci.org/snize/ci_test)
* [https://travis-ci.org/snize/ci_test](https://travis-ci.org/snize/ci_test "Travis CI - Free Hosted Continuous Integration Platform for the Open Source Community")

## drone.io

* [![Build Status](https://drone.io/github.com/snize/ci_test/status.png)](https://drone.io/github.com/snize/ci_test/latest)
* [https://drone.io/github.com/snize/ci_test](https://drone.io/github.com/snize/ci_test "Builds | ci_test")

## wercker

* [![wercker status](https://app.wercker.com/status/c4a98759cc9a89b290431131234a6e8c "wercker status")](https://app.wercker.com/project/bykey/c4a98759cc9a89b290431131234a6e8c)
* [wercker](https://app.wercker.com/#applications/5257ab5df76e8f6003006880/tab/builds "wercker")